#!/bin/sh
set -e

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

mkdir $DIR
tar -xf $3 --strip-components=1 -C $DIR
rm $3
mv $DIR/org.abego.treelayout $DIR/$DIR
XZ_OPT=--best tar -c -v -J -f $TAR -C $DIR \
    --exclude '.settings' \
    --exclude '.classpath' \
    --exclude '.project' \
    --exclude 'nbproject' \
    --exclude 'doc/*' \
    --exclude 'src/website' \
    $DIR
rm -Rf $DIR
